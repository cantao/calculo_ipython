{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Funções"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Configurações"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from sympy import *\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Definição [[2]](#stewart2013)\n",
    "\n",
    "__Função__ é uma lei que associa, a cada elemento $x$ em um conjunto $D$, exatamente um elemento, chamado $f(x)$, em um conjunto $E$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Características [[2]](#stewart2013), [[3]](#thomas2009)\n",
    "\n",
    "- $D$ e $E$ são subconjuntos do conjunto dos números reais;\n",
    "- $D$ é chamado de _domínio da função_, composto por todos os valores possíveis de serem aplicados em $f(x)$, denominado por $\\text{Dom}(f)$;\n",
    "- $E$ é conhecido como _imagem da função_, que é o conjunto de todos os valores possíveis de $f(x)$, denotado por $\\text{Im}(f)$;\n",
    "- O _contra domínio_ são todos os valores que estão na imagem, mas __não__ necessariamente inclui todos os elementos do conjunto $E$;\n",
    "- $f$ uma _regra_ que associa a cada elemento $x$ do domínio $\\text{Dom}(f)$, um único elemento $y=f(x)$ do contra domínio."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> #### Notação\n",
    "> A __regra__ é geralmente escrita como $y=f(x)$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Para que uma relação ou correspondência seja considerada função deve:\n",
    "\n",
    "1. Todos os elementos da origem (domínio) devem ter correspondente no destino (contra-domínio).\n",
    "\n",
    "2. Existe _apenas um elemento_ no destino para cada elemento na origem."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Exemplo de função](pics/dominio_imagem_ex.png \"Ilustração de função usando conjuntos.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Na imagem acima, temos:\n",
    "\n",
    "\\begin{array}{rcl}\n",
    "\\text{Domínio}        & \\quad & A=\\{1,2,3,4\\} \\\\\n",
    "\\text{Contra-domínio} &       & B=\\{2,4,5,6,8\\} \\\\\n",
    "\\text{Imagem}         &       & I=\\{2,4,6,8\\} \\\\\n",
    "\\text{Função}         &       & f(x)=2x\n",
    "\\end{array}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Paridade"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Função par [[2]](#stewart2013)\n",
    "\n",
    "Uma função é par se tivermos $f(x)=f(-x)$ para todos os elementos do domínio. Por exemplo: \n",
    "\n",
    "$$\n",
    "f(x)=x^2\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Função  ímpar [[2]](#stewart2013)\n",
    "\n",
    "Uma função é ímpar se para cada elemento do domínio tivermos $f(-x)=-f(x)$. Por exemplo:\n",
    "\n",
    "$$\n",
    "f(x)=x^3\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Função inversa [[3]](#thomas2009)\n",
    "\n",
    "Seja $f$ uma função _injetora_ com domínio $D$ e imagem $E$. Então, a sua __função inversa__ $f^{-1}$ tem domínio $E$ e imagem $D$ e é definida por\n",
    "\n",
    "$$\n",
    "f^{-1}(y) = x\\quad \\Longleftrightarrow \\quad f(x) = y\n",
    "$$\n",
    "\n",
    "para todo $y$ em $D$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Vale ainda\n",
    "\n",
    "$$\n",
    "\\begin{array}{c}\n",
    "f^{-1}\\left( f(x) \\right ) = x \\quad \\text{ para todo } x \\in D \\\\\n",
    "f\\left ( f^{-1}(x) \\right ) = x \\quad \\text{ para todo } x \\in E\n",
    "\\end{array}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> ## ATENÇÃO!\n",
    ">\n",
    ">__Não__ confunda o $-1$ em $f^{-1}$ como um expoente. Assim\n",
    ">\n",
    ">$$\n",
    "f^{-1} (x) \\neq \\dfrac{1}{f(x)}.\n",
    "$$\n",
    ">\n",
    ">Note que,\n",
    ">\n",
    ">$$\n",
    "\\dfrac{1}{f(x)} = \\left [ f(x) \\right ]^{-1}.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Função composta [[2]](#stewart2013), [[3]](#thomas2009)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A função $f$ composta com a função $g$, que denotamos $f\\circ g$, é a função definida por\n",
    "\n",
    "$$\n",
    "(f\\circ g)(x) = f(g(x)).\n",
    "$$\n",
    "\n",
    "de forma que\n",
    "\n",
    "$$\n",
    "\\text{Dom}(f\\circ g) = \\{x \\in \\text{Dom}(g) \\mid g(x) \\in \\text{Dom}(f)\\}.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.mscabral.pro.br/sitemauro/aulas/Func16.gif\"  width=\"360\" height=\"280\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exemplo\n",
    "\n",
    "Dadas as funções $f(x)=\\sqrt{x}\\,$ e $\\,g(x)=x^2+1$, obtenha a função composta $f{\\circ}g$ usando [SymPy](http://docs.sympy.org/latest/tutorial/intro.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Habilitando o uso de equações (use apenas com uma conexão à Internet!)\n",
    "init_printing()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Não esqueça de dizer que 'x' é uma variável simbólica\n",
    "var(\"x\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Para criar uma função em Python, basta usar a palavra chave `def`, seguida do nome da função (aqui usamos `f` e `g`) e os argumentos de entrada entre parênteses. Neste caso temos apenas a variável `x` como argumento de entrada."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "f = sqrt(x)\n",
    "g = x**2+1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# fog\n",
    "f.subs(x, g)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> ### Exercício\n",
    "> Usando SymPy, obtenha as composições\n",
    ">\n",
    "> - $f(f(x))$\n",
    "> - $g(f(x))$\n",
    "> - $g(g(x))$\n",
    "> - $f(f(f(x)))$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Comportamento de uma função"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Analisando o gráfico de uma função nos eixos cartesianos podemos observar detalhes importantes como:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Raiz ou zero de uma função[[4]](#site)\n",
    "\n",
    "Os pontos onde o gráfico da função corta o eixo do $x$ são chamados de _raízes da função_ e pelo fato de suas ordenadas serem nulas dá-se, também, o nome de _zeros da função_."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exemplo\n",
    "\n",
    "Vamos traçar um gráfico da função $f(x) = x^2-6x+5$ e ver se conseguimos determinar por inspeção os zeros de $f$. Lembrando, para o traçado de gráficos usamos a função `plot()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plot(x**2-6*x+5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Da forma que o gráfico foi criado pelo SymPy, usando o intervalo de $[-10,10]$ não permite uma localização visual razoável dos zeros. Vamos mudar o intervalo para $[0,6]$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plot(x**2-6*x+5, (x,0,6))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Agora podemos ver que os pontos $1$ e $5$ são raízes da função $f(x) = x^2-6x+5$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> #### Exercício\n",
    "> Para confirmar o valor das raízes, use o comando `solve()` do SymPy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "solve(x**2-6*x+5, x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Sinal de uma função\n",
    "\n",
    "A função é dita positiva em $x_0$ se $f(x_0) > 0$ e negativa em $x_0$ se $f(x_0) < 0$. Naturalmente podemos estender estes conceitos para intervalos, dizendo que $f$ é positiva em $[a,b]$ se $f(x)>0, \\forall x \\in [a,b]$ e negativa em $[a,b]$ se $f(x)<0, \\forall x \\in [a,b]$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "No exemplo anterior podemos ver que quando $x<1$ ou $x>5$ a função é positiva e que quando $1<x<5$ a função é negativa."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Função crescente e decrescente\n",
    "\n",
    "Se num determinado intervalo um aumento na variável independente $x$ acarreta num aumento correspondente de $f(x)$, a função é dita crescente. Se, por outro lado, o aumento de $x$ provoca o decréscimo de $f(x)$, então a função é dita decrescente.\n",
    "\n",
    "De [3] temos que uma função $f$ é chamada __crescente__ em um intervalo $I$ se\n",
    "$$\n",
    "f(x_1) < f(x_2) \\quad \\text{ quando } x_1 < x_2 \\text{ em } I.\n",
    "$$\n",
    "É denominada __decresente__ em $I$ se\n",
    "$$\n",
    "f(x_1) > f(x_2) \\quad \\text{ quando } x_1 < x_2 \\text{ em } I.\n",
    "$$\n",
    "\n",
    "Ainda no nosso exemplo, $f(x)=x^2-6x+5$ é crescente para $x>3$ e decrescente para $x<3$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Principais funções"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Função valor absoluto\n",
    "\n",
    "A função valor absoluto $y=|x|$ é definida por\n",
    "\n",
    "$$\n",
    "|x| = \\begin{cases}\n",
    "-x & \\text{se } x< 0 \\\\\n",
    "x & \\text{se } x\\geq 0\n",
    "\\end{cases}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Propriedades do valor absoluto[[4]](#site)\n",
    "\n",
    "1. $|-a|=|a|$\n",
    "2. $|a b|=|a|\\, \\cdot \\, |b|$\n",
    "3. $\\left\\lvert \\dfrac{a}{b} \\right\\rvert=\\dfrac{|a|}{|b|}$\n",
    "4. $|a+b|\\leq |a|+|b|$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Gráfico de y=|x|\n",
    "plot(Abs(x))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Função exponencial [[2]](#stewart2013), [[3]](#thomas2009)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Seja $a \\in \\mathbb{R}, a>0, a\\neq 1$. A função:\n",
    "\n",
    "$$\n",
    "f(x)=a^x\n",
    "$$\n",
    "\n",
    "é uma função exponencial com base $a$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Propriedades\n",
    " \n",
    "Se $a>0$ e $b>0$, as afirmações a seguir são verdadeiras para quaisquer $x, y\\in \\mathbb{R}$:\n",
    "\n",
    "1. $a^x a^y = a^{x+y}$\n",
    "2. $\\dfrac{a^x}{a^y} = a^{x-y}$\n",
    "3. $(a^x)^y = (a^y)^x = a^{xy}$\n",
    "4. $a^x b^x = (ab)^x$\n",
    "5. $\\dfrac{a^x}{b^x} = \\Bigl(\\dfrac{a}{b}\\Bigr)^x$ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### A função exponencial $e^x$[[3]](#thomas2009)\n",
    " \n",
    "Trata-se da um caso particular de função exponencial cuja base é o numero\n",
    "\n",
    "$$\n",
    "e=2,718281828.\n",
    "$$\n",
    "\n",
    "Podemos definir $e$ como o número para o qual tende a função\n",
    "\n",
    "$$\n",
    "f(x) = \\biggl(1+\\frac{1}{x}\\biggr)^x,\n",
    "$$\n",
    "\n",
    "quando $x \\rightarrow \\infty$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Gráfico de y=3**(-x)\n",
    "f = 3**(-x)\n",
    "\n",
    "plot(f, (x,-2,2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Funções logarítimicas [[3]](#thomas2009)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A função logarítimica de base $a$, $y = \\log_a x$ é a função inversa da função exponencial $y=a^x$, $(a>0, a \\neq 1)$ de base $a$.\n",
    "\n",
    "\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Funções trigonométricas [[3]](#thomas2009)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "####Função seno\n",
    "\n",
    "A função trigonométrica seno $y= \\sin x$ possui as seguintes propriedades:\n",
    "\n",
    "- Domínio: $- \\infty < x <\\infty$;\n",
    "- Imagem: $-1 \\leq y \\leq 1$;\n",
    "- Período: $2 \\pi$.\n",
    "\n",
    "Abaixo, o gráfico dessa função.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Não esqueça de dizer que 'x' é uma variável simbólica\n",
    "var('x')\n",
    "\n",
    "plot(sin(x))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "####Função cosseno\n",
    "\n",
    "A função trigonométrica cosseno $y= \\cos x$ possui as seguintes propriedades:\n",
    "\n",
    "- Domínio: $- \\infty < x <\\infty$;\n",
    "- Imagem: $-1 \\leq y \\leq 1$;\n",
    "- Período: $2 \\pi$.\n",
    "\n",
    "Abaixo, o gráfico dessa função."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Não esqueça de dizer que 'x' é uma variável simbólica\n",
    "var(\"x\")\n",
    "\n",
    "plot(cos(x))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "####Função tangente\n",
    "\n",
    "A função trigonométrica tangente $y= \\tan x$ possui as seguintes propriedades:\n",
    "\n",
    "- Domínio: $x \\neq \\pm \\frac{\\pi}{2}, \\pm \\frac{3 \\pi}{2}, ...$;\n",
    "- Imagem: $- \\infty < y < \\infty$;\n",
    "- Período: $ \\pi$.\n",
    "\n",
    "Abaixo, o gráfico dessa função."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plot(tan(x))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> __Explique__ o por quê do gráfico da tangente ter dado errado. Como você faria para corrigi-lo?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "####Função secante\n",
    "\n",
    "A função trigonométrica secante $y= \\sec x$ possui as seguintes propriedades:\n",
    "\n",
    "- Domínio: $x \\neq \\pm \\frac{\\pi}{2}, \\pm \\frac{3 \\pi}{2}, ...$;\n",
    "- Imagem: $ y \\leq -1 \\ \\ $ e $ \\ \\ y \\geq 1 $;\n",
    "- Período: $ 2 \\pi$.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "####Função cossecante\n",
    "\n",
    "A função trigonométrica cossecante $y= \\csc x$ possui as seguintes propriedades:\n",
    "\n",
    "- Domínio: $x \\neq 0, \\pm \\pi, \\pm 2 \\pi, ...$;\n",
    "- Imagem: $ y \\leq -1 \\ \\ $ e $ \\ \\ y \\geq 1 $;\n",
    "- Período: $ 2 \\pi$.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "####Função cotangente\n",
    "\n",
    "A função trigonométrica cotangente $y= \\cot x$ possui as seguintes propriedades:\n",
    "\n",
    "- Domínio: $x \\neq 0, \\pm \\pi, \\pm 2 \\pi, ...$;\n",
    "- Imagem: $ - \\infty <y < \\infty $;\n",
    "- Período: $ \\pi$.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> #### Exercício\n",
    "> Construa os gráficos das funções secante, cossecante e cotangente."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Identidades\n",
    "\n",
    "$$\\DeclareMathOperator{\\sen}{sen} \\DeclareMathOperator{\\tg}{tg} \\DeclareMathOperator{\\cotg}{cotg} \\DeclareMathOperator{\\cossec}{cossec}\n",
    "\\cos^2 \\theta + \\sen^2 \\theta=1\n",
    "$$\n",
    "\n",
    "$$\n",
    "1+\\tg^2 \\theta=\\sec^2 \\theta\n",
    "$$\n",
    "\n",
    "$$\n",
    "1+\\cotg^2 \\theta=\\cossec^2 \\theta\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercícios"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. Durante o estudo de funções podemos notar que o formato de alguns gráficos podem ser similares a algumas letras, como por exemplo podemos perceber que o gráfico de uma função quadrática com concavidade voltada para cima assume uma forma parecida com a letra \"U\". Analogamente, construa 4 gráficos(sendo um para cada letra) que assumam uma forma parecida com as letras da palavra \"LOVE\"."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## Referências Bibliográficas\n",
    "\n",
    "<a id=\"figueiredo2011\"></a>1. V. L. X. Figueiredo, M. P. Mello, S. A. Santos. _\"Cálculo com Aplicações: Atividades Computacionais e Projetos\"_, Editora Ciência Moderna, 2011.\n",
    "\n",
    "<a id=\"stewart2013\"></a>2. J. Stewart. _\"Cálculo\"_, Volume 1, 7$^{\\underline{a}}$ Edição, Cengage Learning, 2013.\n",
    "\n",
    "<a id=\"thomas2009\"></a>3. G. B. Thomas. _\" Cálculo\"_, Volume 1, 11$^{\\underline{a}}$ Edição, Addison Wesley, 2009.\n",
    "\n",
    "<a id=\"site\"></a>4. Entendendo funções. Disponível em: http://www.mscabral.pro.br/sitemauro/aulas/funcao.htm. Acesso em: 15 jun. 2015."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from IPython.core.display import HTML\n",
    "HTML(open('./custom.css', 'r').read())"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.4.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
