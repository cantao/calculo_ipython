{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Derivada de funções trigonométricas, regra da cadeia"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Configurações"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from sympy import *\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Derivadas de funções trigonométricas [[2]](#thomas2009)\n",
    "\n",
    "- A derivada do seno\n",
    "    $$\n",
    "    \\dfrac{d}{dx}(\\sin x) = \\cos x\n",
    "    $$\n",
    "    \n",
    "- A derivada do cosseno\n",
    "    $$\n",
    "    \\dfrac{d}{dx}\\cos x = -\\sin x\n",
    "    $$\n",
    "\n",
    "A derivada das demais funções trigonométricas podem ser obtidas através das derivadas das funções seno e cosseno. Um exemplo é a derivada da função tangente:\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "\\dfrac{d}{dx}(\\tan x) & = \\dfrac{d}{dx}\\biggl( \\dfrac{\\sin x}{\\cos x} \\biggr)\\\\\n",
    "                      & = \\dfrac{\\cos x \\dfrac{d}{dx}(\\sin x)-\\sin x \\dfrac{d}{dx}(\\cos x)}{\\cos^2 x} \\\\\n",
    "                      & =\\dfrac{\\cos x \\cos x - \\sin x(-\\sin x)}{\\cos^2 x} \\\\\n",
    "                      & =\\dfrac{\\cos^2 x +\\sin^2 x}{\\cos^2 x} \\\\\n",
    "                      & =\\dfrac{1}{\\cos^2 x} \\\\\n",
    "                      & =\\sec^ 2 x\n",
    "\\end{aligned}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## Regra da cadeia [[3]](#site)\n",
    "\n",
    "Seja $g$ uma função diferenciável em $x$ e $f$ uma função diferenciável em $g(x)$. A derivada da função composta $f(g(x))$ é o produto da derivada da função externa $f$ (avaliada em $g(x)$) multiplicada pela derivada da função $g$ (avaliada em $x$). Em outras palavras:\n",
    "\n",
    "$$\n",
    "f(g(x))' = f'(g(x))\\, g'(x).\n",
    "$$\n",
    "\n",
    "Sejam $u=g(x)$ e $y=f(u)$ a regra da cadeia também pode ser escrita como: \n",
    "\n",
    "$$\n",
    "\\dfrac{dy}{dx}=\\dfrac{dy}{du} \\dfrac{du}{dx}.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Derivação implícita [[3]](#site)\n",
    "\n",
    "As funções encontradas até agora podem ser descritas expressando-se uma variável explicitamente em termos de outra: em geral, $y=f(x)$. Algumas funções, entretanto, são definidas implicitamente por uma relação entre $x$ e $y$.\n",
    "\n",
    "Felizmente, não precisamos resolver uma equação e escrever $y$ em termos de $x$ para encontrar a derivada de $y$. Em vez disso, podemos usar o método da derivação implícita, que consiste em derivar ambos os lados da equação em relação a $x$ e então isolar $y’$ na equação resultante."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Passos para derivação implícita [[2]](#thomas2009)\n",
    "\n",
    "1. Derive os dois lados da equação em relação a $x$, considerando $y$ como função derivável de $x$.\n",
    "1. Reúna os termos que contém $\\dfrac{dy}{dx}$ em um lado da equação.\n",
    "1. Fatore, isolando $\\dfrac{dy}{dx}$.\n",
    "1. Encontre $\\dfrac{dy}{dx}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exemplo\n",
    "\n",
    "Se $x^2 + y^2 = 25$, encontre dy/dx.\n",
    "\n",
    "1. Derive ambos os lados da equação\n",
    "\n",
    "    $$\n",
    "    \\dfrac{d}{dx}(x^2+y^2)=\\dfrac{d}{dx}(25) \\quad \\Rightarrow \\quad \\dfrac{d}{dx}(x^2)+\\dfrac{d}{dx}(y^2)= \\dfrac{d}{dx} (25)\n",
    "    $$\n",
    "    <br>\n",
    "\n",
    "1. Lembrando que $y$ é uma função de $x$ e usando a __Regra da Cadeia__, temos\n",
    "\n",
    "    $$\n",
    "    \\dfrac{d}{dx}(y^2) = 2y\\dfrac{dy}{dx}.\n",
    "    $$\n",
    "\n",
    "1. Assim, temos\n",
    "\n",
    "    $$\n",
    "    2x+2y\\dfrac{dy}{dx} = 0.\n",
    "    $$\n",
    "\n",
    "1. Agora, isole $\\dfrac{dy}{dx}$ nesta equação\n",
    "\n",
    "    $$\n",
    "    \\dfrac{dy}{dx}=-\\dfrac{x}{y}\n",
    "    $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "<h2>Atenção!</h2>\n",
    "<br>\n",
    "\n",
    "Dada uma função `f` em Python, o comando usado para obter sua derivada é `f.diff(variável)`.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-warning\">\n",
    "<h2>Exercícios [[2]](#thomas2009) - Para entrega!</h2>\n",
    "<br>\n",
    "\n",
    "Calcule a derivada primeira das funções abaixo __manualmente__, depois confira o resultado com Python.\n",
    "\n",
    "<ol>\n",
    "<li>$y=\\sec x$\n",
    "<li>$y=2u^3$, $u=8x-1$\n",
    "<li>$s=\\sin(3\\pi t)+ \\cos(3 \\pi t)$\n",
    "<li>$y= x^2 \\sin^4(x) + x \\cos^{-2}(x)$\n",
    "<li>$f(x)=\\biggl( \\dfrac{\\sin(x)}{1+\\cos(x)} \\biggr)^2$\n",
    "<li>$y\\sin\\biggl( \\dfrac{1}{y}\\biggr)=1-xy$\n",
    "<li>$2xy+y^2=x+y$\n",
    "<li>$x^2=\\dfrac{x-y}{x+y}$\n",
    "<li>$x + \\sin (y)= xy$\n",
    "</ol>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Referências Bibliográficas\n",
    "\n",
    "<a id=\"figueiredo2011\"></a>1. V. L. X. Figueiredo, M. P. Mello, S. A. Santos. _\"Cálculo com Aplicações: Atividades Computacionais e Projetos\"_, Editora Ciência Moderna, 2011.\n",
    "\n",
    "<a id=\"thomas2009\"></a>2. G. B. Thomas. _\"Cálculo\"_, Volume 1, 11$^{\\underline{a}}$ Edição, Addison Wesley, 2009.\n",
    "\n",
    "<a id=\"site\"></a>3. Entendendo funções. Disponível em: http://www.mscabral.pro.br/sitemauro/aulas/funcao.htm. Acesso em: 15 jun. 2015.\n",
    "\n",
    "<a id=\"larson2011\"></a>4. R. Larson. _\"Cálculo Aplicado: curso rápido\"_, 8$^{\\underline{a}}$ Edição, Cengage Learning, 2011."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.4.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
